const suma = require("../suma");

test(`Cuando sumo 1 y 2
        el resultado es 3`, () => {
  expect(1 + 2).toBe(3);
});

test(`Cuando comparo null usando toBeNull
        entonces pasa la prueba`, () => {
  expect(null).toBeNull();
});

test(`Cuando comparo undefined usando toBeUndefined
        entonces pasa la prueba`, () => {
  expect(undefined).toBeUndefined();
});

test(`Cuando comparo algo que evalua como verdad con toBeTruthy()
        entonces pasa la prueba`, () => {
  expect([1, 2]).toBeTruthy();
});

test(`Cuando comparo algo que evalua como falso con toBeFalsy()
        entonces pasa la prueba`, () => {
  expect(0).toBeFalsy();
});

test(`Cuando comparo algo que evalua como falso con toBeFalsy()
        entonces pasa la prueba`, () => {
  expect(0).toBeFalsy();
});

test(`Cuando hago verificaciones con el numero cero
            y son correctas
            las verificaciones salen correctas.`, () => {
  const zero = 0;
  expect(zero).not.toBeNull();
  expect(zero).toBeDefined();
  expect(zero).not.toBeUndefined();
  expect(zero).not.toBeTruthy();
  expect(zero).toBeFalsy();
});