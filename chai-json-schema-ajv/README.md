# curso-testing-con-marlon-zayro

Feature: Retirar dinero de Cuenta de ahorros en Cajero electrónico.

```
Escenario: El monto de retiro es menor al saldo.

Dado que tengo un saldo en la cuenta de ahorros de 100.000 pesos
Cuando pido retiro de 50.000
Entonces el saldo es 50.000 y recibo 50.000.
```

```
Escenario: El monto de retiro es mayor al saldo.

Dado que tengo un saldo en la cuenta de ahorros de 200.000 pesos
Cuando pido retiro de 500.000
Entonces me niegan el retiro por falta de fondos.
```
