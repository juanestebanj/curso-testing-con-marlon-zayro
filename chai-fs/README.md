# capacitacion-pruebas-unitarias
ejercicios practicos

## configurar ambiente
1. crear folder limpio
2. ejecutar el comando **git clone https://github.com/jandres314/capacitacion-pruebas-unitarias.git**
3. ejecutar el comando **cd capacitacion-pruebas-unitarias**
4. inicializar gitflow **git flow init**
5. cambiar rama a develop **git checkout develop**
6. ejecutar el comando **cd chai**
7. ejecutar el comando **npm install**
8. ejecutar el comando **npm run test**
